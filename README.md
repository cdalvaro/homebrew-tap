[![hombrew tap cdalvaro][homebrew_tap_badge]][homebrew_tap_url]

# <img src="https://simpleicons.org/icons/homebrew.svg" height=24pt> Homebrew Custom Tap

[@cdalvaro](https://github.com/cdalvaro) custom formulae for the [Homebrew package manager](https://brew.sh).

## How to enable this tap?

Just type `brew tap cdalvaro/tap`. This will allow you to install formulae from this tap.

You can directly type `brew install cdalvaro/tap/<formula>` to install the specified `<formula>`.

## Available formulae

| Formula &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Description                                                                                                                                                                                                         |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [![catboost][catboost_badge]](Formula/catboost.rb) | [catboost/catboost](https://github.com/catboost/catboost) Fast, scalable, high performance Gradient Boosting on Decision Trees cli tool |
| [![cpp-jwt][cpp-jwt_badge]](Formula/cpp-jwt.rb)                                                                                                                                                                                                                                                 | [arun11299/cpp-jwt](https://github.com/arun11299/cpp-jwt) JSON Web Token library for C++                                                                                                                            |
| [![cpp-plotly][cpp-plotly_badge]](Formula/cpp-plotly.rb)                                                                                                                                                                                                                                        | [pablrod/cppplotly](https://github.com/pablrod/cppplotly) Generate html/javascript charts from C++ data using javascript library plotly.js                                                                          |
| [![cpp-zmq][cpp-zmq_badge]](Formula/cpp-zmq.rb)                                                                                                                                                                                                                                                 | [zeromq/cppzmq](https://github.com/zeromq/cppzmq) Header-only C++ binding for libzmq                                                                                                                                |
| [![howard-hinnant-date][howard-hinnant-date_badge]](Formula/howard-hinnant-date.rb)                                                                                                                                                                                                             | [HowardHinnant/date](https://github.com/HowardHinnant/date) A date and time library based on the C++11/14/17 <chrono> header                                                                                        |
| [![salt][salt_badge]](Formula/salt.rb)                                                                                                                                                                                                                                                          | [saltstack/salt](https://github.com/saltstack/salt) Software to automate the management and configuration of any infrastructure or application at scale.                                                            |
| [![simple-web-server][simple-web-server_badge]](Formula/simple-web-server.rb)                                                                                                                                                                                                                   | [eidheim/Simple-Web-Server](https://gitlab.com/eidheim/Simple-Web-Server) A very simple, fast, multithreaded, platform independent HTTP and HTTPS server and client library implemented using C++11 and Boost.Asio. |
| [![wxmac][wxmac_badge]](Formula/wxmac.rb)                                                                                                                                                                                                                                                       | [wxWidgets/wxWidgets](https://github.com/wxWidgets/wxWidgets) Cross-Platform GUI Library                                                                                                                            |

## More Documentation

More documentation is available at: [Homebrew - Taps](https://docs.brew.sh/Taps)

[homebrew_tap_badge]: https://img.shields.io/badge/brew%20tap-cdalvaro/tap-orange?style=flat-square&logo=Homebrew&color=FBB040
[homebrew_tap_url]: https://github.com/cdalvaro/homebrew-tap
[catboost_badge]: https://img.shields.io/badge/catboost-0.24.4-orange?style=flat-square&color=FBB040
[cpp-jwt_badge]: https://img.shields.io/badge/cpp--jwt-1.4-orange?style=flat-square&color=FBB040
[cpp-plotly_badge]: https://img.shields.io/badge/cpp--plotly-0.4.0-orange?style=flat-square&color=FBB040
[cpp-zmq_badge]: https://img.shields.io/badge/cpp--zmq-4.7.1-orange?style=flat-square&color=FBB040
[howard-hinnant-date_badge]: https://img.shields.io/badge/howard--hinnant--date-3.0.0-orange?style=flat-square&color=FBB040
[salt_badge]: https://img.shields.io/badge/salt-3002.2%20(python@3.7)-orange?style=flat-square&color=FBB040
[simple-web-server_badge]: https://img.shields.io/badge/simple--web--server-3.1.1-orange?style=flat-square&color=FBB040
[wxmac_badge]: https://img.shields.io/badge/wxmac-3.1.4-orange?style=flat-square&color=FBB040
